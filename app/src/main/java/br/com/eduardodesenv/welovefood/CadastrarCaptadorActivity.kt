package br.com.eduardodesenv.welovefood

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class CadastrarCaptadorActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cadastrar_captador)

    }

    fun openLogin(v: View){

        var i = Intent(this,
                LoginActivity::class.java)
        startActivity(i)
    }

}