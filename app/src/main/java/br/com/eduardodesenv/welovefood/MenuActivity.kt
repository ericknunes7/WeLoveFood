package br.com.eduardodesenv.welovefood

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class MenuActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

    }

    /*fun openListDoadores(v: View){

        var i = Intent(this,
                MainActivity::class.java)
        startActivity(i)
    }*/

    fun openListCaptadores(v: View){

        var i = Intent(this,
                MainActivity::class.java)
        startActivity(i)
    }

    fun openFazerDoacao(v: View){

        var i = Intent(this,
                CameraActivity::class.java)
        startActivity(i)
    }


}
