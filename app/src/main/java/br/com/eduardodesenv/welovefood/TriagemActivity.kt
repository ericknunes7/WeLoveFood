package br.com.eduardodesenv.welovefood

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View

class TriagemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_triagem)

    }

    fun openCadDoador(v: View){

        var i = Intent(this,
                CadastrarDoadorActivity::class.java)
        startActivity(i)
    }

    fun openCadCaptador(v: View){

        var i = Intent(this,
                CadastrarCaptadorActivity::class.java)
        startActivity(i)
    }


}