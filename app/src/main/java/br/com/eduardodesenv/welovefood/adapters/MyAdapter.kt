package br.com.eduardodesenv.welovefood.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.eduardodesenv.welovefood.R
import br.com.eduardodesenv.welovefood.models.Captador
import kotlinx.android.synthetic.main.lista_captadores.view.*

class MyAdapter (val context: Context,
                 private val captadores: ArrayList<Captador>,
                 var clickListener:(Captador)->Unit) : RecyclerView.Adapter<MyAdapter.ViewHolder>()

{

    class ViewHolder(itemView: View):
            RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.lista_captadores, parent, false)
        var vh = ViewHolder(v)

        return vh
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var captador = captadores[position]
        holder.itemView.tvNome.text = captador.nome
        holder.itemView.tvEmail.text = captador.email

        holder.itemView.setOnClickListener{clickListener(captador)}
    }

    override fun getItemCount(): Int {
        return captadores.size
    }

}