package br.com.eduardodesenv.welovefood.models

data class Captador (
        var image:String,
        var nome:String,
        var email:String)
