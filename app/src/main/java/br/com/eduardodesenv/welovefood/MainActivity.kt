package br.com.eduardodesenv.welovefood

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import br.com.eduardodesenv.welovefood.adapters.MyAdapter
import br.com.eduardodesenv.welovefood.models.Captador
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvCaptador.setHasFixedSize(true)
        rvCaptador.layoutManager = LinearLayoutManager(this)

        var captadores = ArrayList<Captador>()
        for(i in 1..100){
            captadores.add(
                    Captador("",
                            "Nome $i",
                            "E-mail $i"
                    )
            )
        }

        var adapter = MyAdapter(this, captadores){
            Toast.makeText(this, it.nome, Toast.LENGTH_LONG).show()
        }

        rvCaptador.adapter = adapter

        Log.i("TESTE", SessionController.str1)
        Log.i("TESTE", SessionController.str2)




    }
}
