package br.com.eduardodesenv.welovefood.models

data class Doador (
    var image:String,
    var nome:String,
    var email:String)
