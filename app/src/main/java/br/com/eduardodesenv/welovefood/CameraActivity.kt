package br.com.eduardodesenv.welovefood

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.camera_activity.*

class CameraActivity: AppCompatActivity() {

    val CAMERA_REQUEST_CODE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.camera_activity)

        btnCamera.setOnClickListener{
            val chamarCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if(chamarCameraIntent.resolveActivity(packageManager) != null){
                startActivityForResult(chamarCameraIntent, CAMERA_REQUEST_CODE)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when(requestCode){
            CAMERA_REQUEST_CODE -> {
                if(resultCode == Activity.RESULT_OK && data != null){
                    photoImageView.setImageBitmap(data.extras.get("data") as Bitmap)
                }
            }
            else -> {
                Toast.makeText(this,"Não foi possível iniciar", Toast.LENGTH_SHORT).show()
            }
        }
    }
}