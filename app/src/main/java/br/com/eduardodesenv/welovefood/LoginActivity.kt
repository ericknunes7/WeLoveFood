package br.com.eduardodesenv.welovefood

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    var login  : TextView?=null
    var senha : TextView?=null
    var manterConectado : CheckBox?=null
    var buttonLogin : Button?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login = findViewById(R.id.tvLogin)
        senha = findViewById(R.id.tvSenha)
        manterConectado = findViewById(R.id.cbManterConectado)
        buttonLogin = findViewById(R.id.btnLogin)

        (buttonLogin as Button).setOnClickListener{

            FirebaseAuth.getInstance().signInWithEmailAndPassword((login as TextView).text.toString(), (senha as TextView).text.toString())
                    .addOnCompleteListener {
                        if (it.isSuccessful) {

                            if((manterConectado as CheckBox).isChecked){

                                var prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
                                prefs.putBoolean("manterConectado", true).apply()

                            }else{

                                var prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext).edit()
                                prefs.putBoolean("manterConectado", false).apply()

                            }

                            //chamar proxima Activity

                            // TODO o que acontece em caso de sucesso
                        } else {

                            Toast.makeText(this, "Ops! Aconteceu algum problema", Toast.LENGTH_LONG).show()
                            // TODO o que acontece em caso de em caso de falha
                        }
                    }
        }
    }

    fun openMenu(v: View){

        var i = Intent(this,
                MenuActivity::class.java)
        startActivity(i)
    }

    fun openTriagem(v: View){

        var i = Intent(this,
                TriagemActivity::class.java)
        startActivity(i)
    }
}